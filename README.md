## Overview

Below is a list of my code produced while at IACL. Most of the code exists in 
an installable Python package. I provide a short summary of what the package
does along with some other installation and usage information. For more
details on usage, see the repository along with the documentation.

If you run into a bug, want to request a feature be added, or find the 
documentation unclear with any of the packages, please raise an issue in the 
relevant repository. For non-bug/feature/documentation related queries, contact
me by email at `jcreinhold@gmail.com`

## PyPI

### intensity-normalization

This [package](https://github.com/jcreinhold/intensity-normalization) contains 
various methods to normalize the intensity of various 
modalities of magnetic resonance (MR) images, e.g., T1-weighted (T1-w), 
T2-weighted (T2-w), FLuid-Attenuated Inversion Recovery (FLAIR), and Proton 
Density-weighted (PD-w).

Install with:

```
pip install intensity-normalization
```

Individual time-point normalization methods (command-line scripts to the in
parentheses):

- Z-score (`zscore-normalize`)
- Fuzzy C-means (FCM)-based tissue mean (`fcm-normalize`)
- Gaussian Mixture Model (GMM)-based WM mean (`gmm-normalize`)
- Kernel Density Estimate (KDE) WM mode (`kde-normalize`)
- WhiteStripe (`ws-normalize`)

Sample-based normalization methods:

- Least squares (LSQ) tissue mean (`lsq-normalize`)
- Piecewise Linear Histogram Matching (`nyul-normalize`)
- RAVEL (`ravel-normalize`)

Example usage:

```
fcm-normalize -i t1/ -m masks/ -o test_fcm -v
```

where `t1/` is a directory full of N T1-w images and `masks/` is a directory 
full of N corresponding brain masks, `test_fcm` is the output directory for the 
normalized images, and `-v` controls the verbosity of the output.

This package was used in support of the paper ["Evaluating the Impact of 
Intensity Normalization on MR Image Synthesis"](https://arxiv.org/abs/1812.04652).

### tiramisu-brulee

A 2D and 3D PyTorch [implementation](https://github.com/jcreinhold/tiramisu-brulee) 
of the Tiramisu CNN. This package is primarily used for multiple sclerosis (MS) 
lesion segmentation; specifically, T2 lesions in the brain but can be used for
any segmentation task.

Install with:

```
pip install tiramisu-brulee
```

Or, for the full CLI for lesion segmentation, install with:

```
pip install "tiramisu-brulee[lesionseg]"
```

Import the 2D or 3D Tiramisu model with to use with your own setup:

```python
from tiramisu_brulee.model import Tiramisu2d, Tiramisu3d
```

Or use the CLI (if you installed with `[lesionseg]`) with 

```
lesion-train ...
lesion-predict ...
lesion-predict-image ...
```

See [this exercise](https://gist.github.com/jcreinhold/3442ce5ead5cfe626d8b072e77a24a73)
to get started (the exercise directory is located at `/iacl/pg20/jacobr/tiramisu_exercise`
and should be copied to your personal directory before using!). Use the CLI with 
the `--help` flag to list the arguments and options for the command line scripts
or check out the documentation for more details.

### pyrobex

Python bindings for ROBEX brain extraction. This 
[package](https://github.com/jcreinhold/pyrobex) comes with ROBEX v1.2 for 
Linux. Windows is not currently supported.

Install with:

```
pip install pyrobex
```

Basic usage:

```
robex path/to/t1w_image.nii -os path/to/stripped.nii -om path/to/mask.nii
```

The real use-case of this package is by importing robex and using it within
another script or neuroimaging pipeline, e.g.,

```python
import nibabel as nib
from pyrobex.robex import robex
image = nib.load('path/to/t1w_image.nii')
stripped, mask = robex(image)
```

### lesion-metrics

Python [package](https://github.com/jcreinhold/lesion-metrics) implementing
various metrics for evaluating lesion segmentations against ground truth.

Install with:

```
pip install lesion-metrics
```

Basic usage:

```python
import nibabel as nib
from lesion_metrics import dice
pred = nib.load('pred_label.nii.gz').get_fdata()
truth = nib.load('truth_label.nii.gz').get_fdata()
dice_score = dice(pred, truth)
```

Or use the CLI to generate a report for two sets of directories of
predictions and grouth truth segmentation:

```
lesion-metrics -p predictions/ -t truth/ -o output.csv
```

---

## Gitlab

### REPLICA

Given a set of training images of different MR contrasts (an atlas), train a 
random forest regressor to output values of a target MR contrast. Can use single 
resolution or multiresolution features (although the inputs to either pathway 
are different!). You can generate/see some (very sparse) documentation in the 
docs/ directory.

Note that this MATLAB [package](https://gitlab.com/jcreinhold/replica) is not 
actively maintained and has been superseded by the synthit python package 
(discussed below).

### synthit

This [package](https://gitlab.com/jcreinhold/synthit) contains *non*-deep neural 
network-based code to synthesize magnetic resonance (MR) and computed tomography 
(CT) brain images. Synthesis is the procedure of learning the transformation 
that takes a specific contrast image to another estimate contrast.

```
pip install git+git://github.com/jcreinhold/synthit.git
```

### synthtorch

This [package](https://gitlab.com/jcreinhold/synthnn) contains deep neural 
network-based (pytorch) modules to synthesize magnetic resonance (MR) and 
computed tomography (CT) brain images. Synthesis is the procedure of learning 
the transformation that takes a specific contrast image to another estimate 
contrast.

```
pip install git+git://github.com/jcreinhold/synthtorch.git
```

### annom

This [package](https://gitlab.com/jcreinhold/annom) provides functions 
associated with anomaly detection, especially related to pytorch. This package 
extends the functionality of synthtorch.

```
pip install git+git://github.com/jcreinhold/annom.git
```

Used in support of the papers: 
["Validating uncertainty in medical image translation"](https://arxiv.org/abs/2002.04639)
and
["Finding novelty with uncertainty"](https://arxiv.org/abs/2002.04626)

### synthqc

This [package](https://gitlab.com/jcreinhold/synthqc) supports a suite of 
quality analysis programs for MR/CT image synthesis. This package was mostly 
used in support of the paper ["Evaluating the Impact of Intensity Normalization 
on MR Image Synthesis"](https://arxiv.org/abs/1812.04652).

```
pip install git+git://github.com/jcreinhold/synthqc.git
```

### klon

This [package](https://gitlab.com/jcreinhold/klon) contains a (mostly failed 
attempt) to conduct style transfer between MR images from different sets using 
a model of MR pulse sequences, i.e., an update to PSICLONE.

```
pip install git+git://github.com/jcreinhold/klon.git
```

### tiramisu-lesionseg

This [package](https://gitlab.com/jcreinhold/lesionseg) is a version of the 
state-of-the-art MS lesion segmentation 2.5D (i.e., 3 adjacent slices are used 
as input) Tiramisu method from Vanderbilt. This version allows you to segment 
images with arbitrary input images in a simple-to-use interface. (Previously it 
was set up only to run datasets in a very particular way.)

```
pip install git+git://gitlab.com/jcreinhold/lesionseg.git
```

### slant

This [package](https://gitlab.com/jcreinhold/slant) is a stream-lined version of 
the SLANT whole-brain segmentation method. It was not tested extensively.

### iacl-mni-atlas

This [repository](https://gitlab.com/jcreinhold/iacl-mni-atlas) contains the 
IACL version of the MNI ICBM 2009c symmetric T1-w atlas with 0.8mm isotropic 
resolution.

---

## Github

### uncertaintorch

This [package](https://github.com/jcreinhold/uncertaintorch) contains deep 
neural network-based (pytorch) modules to synthesize or segment magnetic 
resonance (MR) and computed tomography (CT) brain images with uncertainty 
estimates (e.g., epistemic and aleatory uncertainty).

```
pip install git+git://github.com/jcreinhold/uncertaintorch.git
```

### niftidataset

This [package](https://github.com/jcreinhold/niftidataset) simply provides 
appropriate dataset and transforms classes for NIfTI images for use with PyTorch 
or PyTorch wrappers.

Note that this package is required for synthtorch, but otherwise shouldn't
be used. [TorchIO](https://torchio.readthedocs.io/) is a better project
for this with more support and is actively maintained.

```
pip install git+git://github.com/jcreinhold/niftidataset.git
```

### counterfactualms

This [repository](https://github.com/jcreinhold/counterfactualms) holds code to 
generate counterfactual images of MR brain images for people with (and without) 
MS using a *structural causal model* (SCM) built in Pyro.

This code was used to generate the counterfactual images in our paper 
["A Structural Causal Model of MR Images of Multiple Sclerosis"](https://arxiv.org/abs/2103.03158).

```
pip install git+git://github.com/jcreinhold/counterfactualms.git
```

---

## Miscellaneous 

I also have a host of other notebooks for analysis and toy problems as well
as scripts hosted on [gist](https://gist.github.com/jcreinhold).

## Pipeline architecture

Our current neuroimaging pipeline is difficult to use because the input format 
is very strict and non-standard outside of IACL. For research quality data, 
it also has a fairly low yield rate; the pipeline fails on around 5 to 10 percent 
of the data---in the first several steps alone---in my experience. 

The current setup of the pipeline is such that most of the code consists of
infrastructure to open, move, and close files that are generated by ANTs, JIST
modules, and other neuroimaging software that is accessed through command-line 
scripts. However, this is unnecessary because Python bindings have been created 
for the majority of the pipeline. ANTs has officially-supported Python bindings 
(see [antspy](https://github.com/ANTsX/ANTsPy)) which can handle registration 
and N4, among other steps, in Python without first saving the files elsewhere 
and manipulating them (or, at least, it abstracts away the saving of 
intermediate files). ROBEX has Python bindings ([pyrobex](https://github.com/jcreinhold/pyrobex)) 
for T1-w brain extraction. SLANT can be modified to better integrate with 
Python. There also appears to be support for CRUISE with [nighres](https://github.com/nighres/nighres).

While there may be some modules remaining that require Python bindings to be
created, that is not a substantial amount of work. If it is extremely difficult
to create Python bindings for some module, there is likely an alternative 
available that already has bindings or is much easier to write bindings for.

If the entire pipeline is written in Python, debugging will be easier 
because calls to external libraries will either be limited or wrapped with
meaningful error handling. Quality assurance can be more easily integrated with 
existing Python packages for that purpose (e.g., [mriqc](https://github.com/poldracklab/mriqc)).

Nipype is an noble effort to create an interface for neuroimaging pipelines, but 
its implementation and usage is not pythonic so is arguably both unintuitive for 
Python programmers and difficult to debug. Also, libraries like nipype should 
reduce boilerplate; however, in my experience, code using nipype requires a 
massive amount of boilerplate and still requires all of the previously mentioned
infrastructure to open, move, and close files. My guess is that there would be 
less boilerplate and infrastructure to write if you simply wrote a Python script 
with calls to the relevant neuroimaging package via the subprocess standard 
library Python package. 

If Python bindings are created for the relevant submodules of the pipeline, 
the pipeline could be reduced to something like the following:

```python
from itertools import accumulate
from pipeline import *


config = PipelineConfig.from_json('config.json', resume=True)

# each function in pipeline takes a (image, config) pair
# and returns a (image, config) pair
pipeline = [
    baseline,
    n4,
    coregister,
    ssr,
    finetune,
    harmonize,
    lesion_seg,
    slant,
    dti_processing,
]

def run_pipeline(x):
    """ returns a list with all intermediate (and final) results """
    return accumulate(pipeline, lambda res, f: f(res), inital=x)


if __name__ == "__main__":
    for image in config.images();
        results = run_pipeline(image)
        generate_report(results)
```

By virtue of the setup, this is easier to debug and alter because the functions
have a common interface and---while the code for each module may have its own
repository---the actual pipeline code spans only one or a handful of files.

If the pipeline could be simplified to such a form, it would be simple to
make modifications to the pipeline for a specific purpose (e.g., want
lesion segmentation but don't want to super-resolve the images). It
would also reduce the problem of data-wrangling to generating an appropriate
configuration file. Generating a properly setup .json or .yaml configuration 
file for a set of data is relatively straight-forward in Python and reduces
the reliance on using Bash to sort the files into the non-standard format
(i.e., not BIDS) that is currently required by the pipeline.
